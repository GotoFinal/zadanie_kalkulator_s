Required: java 11

To compile: `./gradlew bootJar`
To run after compiling: 
```
cd salary-backend/build/libs/
java -jar salary-backend-0.0.1-SNAPSHOT.jar
```