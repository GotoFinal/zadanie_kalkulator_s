package dev.gotofinal.recrutation.app.calc

import dev.gotofinal.recrutation.app.calc.api.SalaryResponseDto
import reactor.core.publisher.Mono

class SalaryCalculationUnitTest extends SalaryCalculatorUnitTestSpecification {
    static Currency EUR = Currency.getInstance("EUR")
    
    def "should calculate net monthly salary by gross daily salary"() {
        given: "EUR is worth 2 PLN"
            1 * currencyClient.toPLN(EUR) >> Mono.just(2d)
        when: "we ask for net salary for germany with 1000 per day"
            SalaryResponseDto response = facade.convertToPLNSalary("German", 1000).block()
        then: "we get correct response and client is called only once"
            response.netSalary == 39400
    }

    def "should not call currency client for PLN"() {
        when: "we ask for net salary for poland with 1000 per day"
            SalaryResponseDto response = facade.convertToPLNSalary("Poland", 1000).block()
        then: "we get correct response"
            response.netSalary == 17400
        and: "currency client was not called"
            0 * currencyClient.toPLN(Currency.getInstance("PLN"))
    }
}