package dev.gotofinal.recrutation.app.calc.infrastructure

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import dev.gotofinal.recrutation.app.calc.api.SalaryResponseDto
import org.junit.Before
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.TestPropertySource
import org.springframework.test.web.reactive.server.WebTestClient
import spock.lang.Specification

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
@TestPropertySource(properties = "currency-client.uri=http://localhost:8089")
class SalaryCalculationIntegrationSpec extends Specification {

    @Autowired
    private WebTestClient webTestClient

    private WireMockServer wireMockServer = new WireMockServer(8089)

    @Before
    void setupStubs() {
        wireMockServer.start()
        wireMockServer.stubFor(WireMock.get(urlEqualTo("/EUR"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                        .withBody('{"table":"A","currency":"euro","code":"EUR","rates":[{"no":"23","effectiveDate":"2019-12-01","mid":3.50}]}')))
    }

    def "should be able to ask for currency"() {
        when:
            WebTestClient.ResponseSpec response = this.webTestClient.get()
                    .uri("/salary?country=Germany&grossPerDay=1000")
                    .accept(MediaType.APPLICATION_JSON)
                    .exchange()

        then:
            SalaryResponseDto responseDto = response.expectStatus().isOk()
                    .returnResult(SalaryResponseDto.class)
                    .getResponseBody()
                    .blockFirst()
            responseDto.netSalary == 58800
    }
}