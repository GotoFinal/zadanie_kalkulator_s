package dev.gotofinal.recrutation.app.calc

import dev.gotofinal.recrutation.app.infrastructure.currency.CurrencyClient
import spock.lang.Specification 

class SalaryCalculatorUnitTestSpecification extends Specification {
    CurrencyClient currencyClient = Mock()
    SalaryCalculatorProperties properties = new SalaryCalculatorProperties([
            new SalaryCalculatorProperties.CountryProperties(
                    "Poland",
                    Currency.getInstance("PLN"),
                    0.20, 200
            ),
            new SalaryCalculatorProperties.CountryProperties(
                    "German",
                    Currency.getInstance("EUR"),
                    0.10, 100
            )
    ])
    SalaryCalculatorFacade facade = new SalaryCalculatorConfiguration().salaryCalculatorFacade(currencyClient, properties)
}