package dev.gotofinal.recrutation.app.infrastructure.currency;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Currency;
import java.util.List;

class RestCurrencyClient implements CurrencyClient {
    private final WebClient webClient;

    RestCurrencyClient(WebClient webClient) {
        this.webClient = webClient;
    }

    @Override
    public Mono<Double> toPLN(Currency currency) {
        return this.webClient
                .get()
                .uri(currency.getCurrencyCode())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .flatMap(response -> response.bodyToMono(CurrencyResponse.class))
                .map(this::extractCurrencyRate);
    }

    private double extractCurrencyRate(CurrencyResponse response) {
        List<CurrencyResponse.CurrencyRate> rates = response.getRates();
        if (rates.isEmpty()) {
            throw new IllegalStateException("Expected at lest one rate per currency");
        }
        return rates.get(0).getMid();
    }

    private static class CurrencyResponse {
        private final List<CurrencyRate> rates;

        CurrencyResponse(@JsonProperty("rates") List<CurrencyRate> rates) {
            this.rates = rates;
        }

        List<CurrencyRate> getRates() {
            return this.rates;
        }

        private static class CurrencyRate {
            private final double mid;

            CurrencyRate(@JsonProperty("mid") double mid) {
                this.mid = mid;
            }

            double getMid() {
                return this.mid;
            }
        }
    }
}
