package dev.gotofinal.recrutation.app.calc.infrastructure;

import dev.gotofinal.recrutation.app.calc.SalaryCalculatorFacade;
import dev.gotofinal.recrutation.app.calc.api.SalaryResponseDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("salary")
class SalaryEndpoint {
    private final SalaryCalculatorFacade facade;

    SalaryEndpoint(SalaryCalculatorFacade facade) {
        this.facade = facade;
    }

    @GetMapping
    public Mono<SalaryResponseDto> getPLNSalary(
            @RequestParam String country,
            @RequestParam double grossPerDay
    ) {
        return this.facade.convertToPLNSalary(country, grossPerDay);
    }
}
