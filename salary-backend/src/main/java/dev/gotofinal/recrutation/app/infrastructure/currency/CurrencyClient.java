package dev.gotofinal.recrutation.app.infrastructure.currency;

import reactor.core.publisher.Mono;

import java.util.Currency;

public interface CurrencyClient {
    Mono<Double> toPLN(Currency currency); 
}
