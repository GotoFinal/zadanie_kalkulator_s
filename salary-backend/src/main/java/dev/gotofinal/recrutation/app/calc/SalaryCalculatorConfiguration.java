package dev.gotofinal.recrutation.app.calc;

import dev.gotofinal.recrutation.app.infrastructure.currency.CurrencyClient;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(SalaryCalculatorProperties.class)
class SalaryCalculatorConfiguration {
    @Bean
    SalaryCalculatorFacade salaryCalculatorFacade(
            CurrencyClient currencyClient,
            SalaryCalculatorProperties properties
    ) {
        SalaryCalculator salaryCalculator = new SalaryCalculator(currencyClient, properties);
        return new SalaryCalculatorFacade(salaryCalculator);
    }
}
