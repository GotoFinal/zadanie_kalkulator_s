package dev.gotofinal.recrutation.app.calc;

import dev.gotofinal.recrutation.app.infrastructure.currency.CurrencyClient;
import reactor.core.publisher.Mono;

import java.util.Currency;

class SalaryCalculator {
    private static final int WORKDAYS_IN_MONTH = 22;
    private static final Currency PLN = Currency.getInstance("PLN");

    private final CurrencyClient currencyClient;
    private final SalaryCalculatorProperties calculatorProperties;

    SalaryCalculator(CurrencyClient currencyClient, SalaryCalculatorProperties calculatorProperties) {
        this.currencyClient = currencyClient;
        this.calculatorProperties = calculatorProperties;
    }

    Mono<Double> calculateSalaryInPLN(String country, double grossPerDay) {
        SalaryCalculatorProperties.CountryProperties countryProperties = this.calculatorProperties.getFor(country);
        return this.getRate(countryProperties)
                .map(rate -> this.calculateSalary(rate, grossPerDay, countryProperties));
    }

    private Mono<Double> getRate(SalaryCalculatorProperties.CountryProperties properties) {
        if (properties.getCurrency().equals(PLN)) {
            return Mono.just(1d);
        }
        return this.currencyClient.toPLN(properties.getCurrency());
    }

    private double calculateSalary(double rate, double grossPerDay, SalaryCalculatorProperties.CountryProperties properties) {
        double grossPerMonth = grossPerDay * WORKDAYS_IN_MONTH;
        double netPerMonth = grossPerMonth * (1 - properties.getTax());
        double netPerMonthAfterFixedCost = netPerMonth - properties.getFixedCost();
        return netPerMonthAfterFixedCost * rate;
    }
}
