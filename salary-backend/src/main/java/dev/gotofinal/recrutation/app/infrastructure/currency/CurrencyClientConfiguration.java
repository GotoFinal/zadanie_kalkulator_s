package dev.gotofinal.recrutation.app.infrastructure.currency;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@EnableConfigurationProperties(CurrencyClientProperties.class)
class CurrencyClientConfiguration {
    @Bean
    public CurrencyClient currencyClient(CurrencyClientProperties properties) {
        return new RestCurrencyClient(
                WebClient.create(properties.getUri())
        );
    }
}
