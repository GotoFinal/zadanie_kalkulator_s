package dev.gotofinal.recrutation.app.infrastructure.currency;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@ConfigurationProperties(prefix = "currency-client")
@ConstructorBinding
class CurrencyClientProperties {
    private final String uri;

    CurrencyClientProperties(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return this.uri;
    }
}