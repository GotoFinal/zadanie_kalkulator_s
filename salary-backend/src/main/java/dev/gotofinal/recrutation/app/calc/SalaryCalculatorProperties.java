package dev.gotofinal.recrutation.app.calc;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.util.Currency;
import java.util.List;
import java.util.StringJoiner;

@ConfigurationProperties(prefix = "salary-calculator")
@ConstructorBinding
class SalaryCalculatorProperties {
    private final List<CountryProperties> countries;

    public SalaryCalculatorProperties(List<CountryProperties> countries) {
        this.countries = countries;
    }

    public List<CountryProperties> getCountries() {
        return this.countries;
    }
    
    CountryProperties getFor(String country) {
        for (CountryProperties countryProperties : this.countries) {
            if (countryProperties.getName().equals(country)) {
                return countryProperties;
            }
        }
        return null;
    }

    public static class CountryProperties {
        private final String name;
        private final Currency currency;
        private final double tax;
        private final double fixedCost;

        public CountryProperties(String name, Currency currency, double tax, double fixedCost) {
            this.name = name;
            this.currency = currency;
            this.tax = tax;
            this.fixedCost = fixedCost;
        }

        public String getName() {
            return this.name;
        }

        public Currency getCurrency() {
            return this.currency;
        }

        public double getTax() {
            return this.tax;
        }

        public double getFixedCost() {
            return this.fixedCost;
        }

        @Override
        public String toString() {
            return new StringJoiner(", ", CountryProperties.class.getSimpleName() + "[", "]")
                    .add("name='" + name + "'")
                    .add("currency=" + currency)
                    .add("tax=" + tax)
                    .add("fixedCost=" + fixedCost)
                    .toString();
        }
    }
}