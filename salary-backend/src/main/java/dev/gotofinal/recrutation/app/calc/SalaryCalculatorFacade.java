package dev.gotofinal.recrutation.app.calc;

import dev.gotofinal.recrutation.app.calc.api.SalaryResponseDto;
import reactor.core.publisher.Mono;

public class SalaryCalculatorFacade {
    private final SalaryCalculator salaryCalculator;

    public SalaryCalculatorFacade(SalaryCalculator salaryCalculator) {
        this.salaryCalculator = salaryCalculator;
    }

    public Mono<SalaryResponseDto> convertToPLNSalary(String country, double grossPerDay) {
        return this.salaryCalculator.calculateSalaryInPLN(country, grossPerDay)
                .map(SalaryResponseDto::new);
    }
}
