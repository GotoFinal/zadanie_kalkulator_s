package dev.gotofinal.recrutation.app.calc.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.StringJoiner;

public class SalaryResponseDto {
    private final double netSalary;

    public SalaryResponseDto(@JsonProperty("netSalary") double netSalary) {
        this.netSalary = netSalary;
    }

    public double getNetSalary() {
        return this.netSalary;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SalaryResponseDto.class.getSimpleName() + "[", "]")
                .add("netSalary=" + netSalary)
                .toString();
    }
}
