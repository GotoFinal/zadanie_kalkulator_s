export class SalaryInput {
  constructor(
    public grossPerDay: number,
    public country: string,
  ) {
  }
}
