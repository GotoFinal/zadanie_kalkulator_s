import {Injectable} from '@angular/core';
import {Salary} from './salary';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {SalaryInput} from "./salaryInput";

@Injectable({
  providedIn: 'root'
})
export class SalaryService {

  constructor(private http: HttpClient) {
  }

  getSalary(salaryInput: SalaryInput): Observable<Salary> {
    let params = new HttpParams().set("country", salaryInput.country).set("grossPerDay", String(salaryInput.grossPerDay));
    return this.http.get<Salary>('/salary', {
      headers: new HttpHeaders({'Content-Type': 'application/json'}),
      params: params
    }).pipe(
      catchError(this.handleError<Salary>())
    );
  }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
