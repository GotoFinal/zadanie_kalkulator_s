export class Salary {
  netSalary: number;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}

