import {Component} from '@angular/core';

import {SalaryInput} from '../salaryInput';
import {SalaryService} from "../salary.service";
import {Salary} from "../salary";

@Component({
  selector: 'app-salary-form',
  templateUrl: './salary-form.component.html',
  styleUrls: ['./salary-form.component.css']
})
export class SalaryFormComponent {

  countries = ['Poland', 'Germany', 'United Kingdom'];
  model = new SalaryInput(400, this.countries[0]);
  salary: Salary = null;

  constructor(private salaryService: SalaryService) {
  }

  onSubmit() {
    this.salaryService.getSalary(this.model).subscribe(
      (salary) => {
        this.salary = salary;
      }
    );
  }
}
