import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {SalaryFormComponent} from './salary-form/salary-form.component';

@NgModule({
  imports: [
    HttpModule,
    HttpClientModule,
    BrowserModule,
    FormsModule
  ],
  declarations: [
    AppComponent,
    SalaryFormComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
